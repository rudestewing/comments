<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Post;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::middleware('auth')->group(function(){
    Route::get('test/{id}',function($id){
        $data = Post::find($id);
        
        return view('test',[
            'data' => $data
        ]);
    });
    Route::get('post',['uses'=>'PostController@index'])->name('post');
    
    Route::get('post/create',['uses'=>'PostController@create'])->name('post.create');
    Route::post('post/create',['uses'=>'PostController@save'])->name('post.save');

    Route::get('post/{id}',['uses'=>'PostController@show'])->name('post.show');

    Route::get('post/edit/{id}',['uses'=>'PostController@edit'])->name('post.edit');
    Route::put('post/edit/{id}',['uses'=>'PostController@update'])->name('post.update');

    Route::get('post/delete/{id}',['uses'=>'PostController@delete'])->name('post.delete');

});
Route::get('/home', 'HomeController@index')->name('home');


