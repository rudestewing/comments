<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('post/{id}/comments',['uses'=>'CommentController@index']);

Route::middleware('auth:api')->group(function() {
    Route::post('/post/{id}/comment', 'CommentController@save');
});
