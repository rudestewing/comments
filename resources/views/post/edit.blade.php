@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <h3 class="display-5">Edit Post</h3>
    </div>
    <form action="{{route('post.update',['id'=>$data->id])}}" method="post">
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" name="title" class="form-control col-xs-12 col-sm-12 col-md-6 col-lg-6" value="{{$data->title}}">
        </div>

        <div class="form-group">
            <label for="">Content</label>
            <textarea name="content" class="form-control col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" cols="30" rows="5">{!! $data->content !!}
            </textarea>
        </div>
        <div class="form-group">
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="published" value="1" id="exampleCheck1"
                @if($data->published == 1) checked @endif>
                <label class="form-check-label" for="exampleCheck1">Publish post </label>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-md btn-success">Update Post</button>
        </div>
    {{csrf_field()}}
    {{method_field('put')}}
    </form>
</div>
@stop
