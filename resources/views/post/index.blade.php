@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <a href="{{route('post.create')}}" class="btn btn-md btn-secondary">Create New Post</a>
    </div>
    <hr>
    <div class="row">
        <h2 class="display-5"> Post List </h2>
    </div>
    <table class="table">
        <thead>
            <tr>
                <td scope="col"> ID </td>
                <td scope="col"> Title </td>
                <td scope="col"> Content </td>
                <td scope="col"> Published</td>
                <td scope="col" colspan="3"> Action </td>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $row)
            <tr @if($row->published == '0') class="table-danger" @endif>
                <td scope="col">{{$row->id}}</td>
                <td scope="col">{{$row->title}}</td>
                <td scope="col">{{$row->content}}</td>
                <td scope="col">
                    @if($row->published == 1)
                        yes
                    @else 
                        nope
                    @endif
                </td>
                <td scope="col"><a href="{{route('post.show',['id'=>$row->id])}}" class="btn btn-sm btn-primary">Show Post</a></td>
                <td scope="col"><a target="_blank" href="{{route('post.edit',['id'=>$row->id])}}" class="btn btn-sm btn-warning">Edit Post</a></td>
                <td scope="col"><a onclick="return confirm('Are You Sure?')"href="{{route('post.delete',['id'=>$row->id])}}" class="btn btn-sm btn-danger">Delete Post</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{$data->links()}}

</div>

@stop

