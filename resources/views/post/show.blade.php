@extends('layouts.app')

@section('content')

<div class="container">
    <div v-for="item in items">
        <p>@{{item.name}}</p>
        <p>@{{item.value}}</p>
    </div>
    <button class="btn btn-default btn-sm" v-on:click="addItem"> Add item in arrays</button>
</div>
<div class="container">
    <h3 class="display-5">{{$data->title}}</h3>
    <p> last update - {{$data->updated_at->toFormattedDateString()}} </p>
    
    @if($data->published == '1')
        <span class="badge badge-success" style="margin-left:15px;">Published</span>
    @else
        <span class="badge badge-default" style="margin-left:15px;">Not Published Yet</span>
    @endif
    <hr>
    <p class="lead">
        {{$data->content}}
    </p>

    <h4>Comments</h4>
    <div style="margin-bottom:50px;">
      <textarea class="d-block form-control" rows="3" placeholder="Leave a comment" v-on:keydown.enter="postComment" v-model="commentBox"></textarea>
      <button class="btn btn-success" style="margin-top:10px" v-on:click.prevent="postComment">Post Comment</button>
    </div>

    <div class="media" v-for="(comment,index) in comments" v-bind:key="index" style="margin-bottom:10px;">
        <img class="mr-3" src="{{url('images/avatar/1.png')}}" alt="Generic placeholder image" style="width:64px; height:64px;">
        <div class="media-body">
            <h5 class="mt-0">@{{comment.user.name}} said.. </h5>
            @{{comment.body}}
        </div>
        <span style="color:#aaa;"> @{{comment.created_at}} </span>
    </div>
</div>
@stop


@section('scripts')
<script>
    const app = new Vue({
        el : '#app',
        data : {
            comments : [],
            commentBox : '',
            post : {!! $data->toJson() !!},
            user : {!! Auth::user()->toJson() !!},
            items : [],
        },
        mounted(){
          this.getComments();  
          this.listen();
          
        },
        methods : {
            addItem(){
                this.comments.unshift({
                    id: '1',
                    name : 'jackoasdasd',
                    value : 1000001231
                });
            },
            getComments() {
                axios
                    .get('/api/post/'+this.post.id+'/comments')
                    .then((response) => {
                        this.comments = response.data;
                        console.log(this.comments);
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            },
            postComment() {
                if(this.commentBox != ''){
                    axios
                        .post('/api/post/'+this.post.id+'/comment',{
                            api_token : this.user.api_token,
                            body : this.commentBox
                        })
                        .then((response) => {
                            this.comments.unshift(response.data);
                            this.commentBox = '';
                        })
                        .catch(function(error){
                            console.log(error);
                        });
                } else {
                    alert('comment required');
                }
            },
            listen(){
                Echo
                    .channel('post.'+this.post.id)
                    .listen('NewComment',(comment) => {
                        if(comment.user.id !== this.user.id ){
                            this.comments.unshift(comment);
                        }else {
                            console.log(comment);
                        }
                    })
            }
        }
    });
</script>
@stop

