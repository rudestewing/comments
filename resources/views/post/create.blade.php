@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <h2 class="display-5">Create New Post</h2>
    </div>
    <hr>
    <form action="{{route('post.save')}}" method="post">
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" name="title" class="form-control col-xs-12 col-sm-12 col-md-6 col-lg-6">
        </div>

        <div class="form-group">
            <label for="">Content</label>
            <textarea name="content" class="form-control col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" cols="30" rows="5"></textarea>
        </div>
        <div class="form-group">
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="published" value="1" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Publish post</label>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-md btn-primary">Save Post</button>
        </div>
    {{csrf_field()}}
    </form>
</div>
@stop


