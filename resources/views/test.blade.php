<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <form action="{{url('/api/post/'.$data->id.'/comment')}}" method="post">
        {{csrf_field()}}
        post comment to : {{$data->id}}  -  {{$data->title}}
        <br>

        <input type="text" name="body" >
        <button type="submit" class="btn btn-primary btn-md">submit</button>
    </form>
</body>
</html>