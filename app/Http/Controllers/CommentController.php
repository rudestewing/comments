<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Comment;

use Auth;

use App\Events\NewComment;

class CommentController extends Controller
{   
    public function index($id)
    {
        $post = Post::find($id);

        return response()->json($post->comment()->with('user')->latest()->get());
    }

    public function save(Request $req,$id)
    {
        $post = Post::find($id);

        $comment = new Comment;
        $comment->body = $req->body;
        $comment->user_id = Auth::guard('api')->user()->id;
        $comment->post_id = $post->id;
        $comment->save();

        $comment = Comment::where('id',$comment->id)->with('user')->first();

        event(new NewComment($comment));

        return $comment->toJson();
    }

}
