<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

use Auth;

class PostController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $data = Post::orderBy('id','DESC')->paginate(10);
        
        return view('post.index',[
            'data' => $data
        ]);
    }

    public function create()
    {
        return view('post.create');
    }

    public function save(Request $req)
    {
        $post = new Post;
        $post->title = $req->title;
        $post->content = $req->content;
        $post->user_id = Auth::user()->id;
        if($req->published == '1'){
            $post->published = '1';
        } else {
            $post->published = '0';
        }

        $post->save();

        return redirect()->route('post');

    }

    public function show($id)
    {
        $data = Post::find($id);

        return view('post.show',[
            'data' => $data
        ]);
    }

    public function edit($id)
    {
        $data = Post::find($id);

        return view('post.edit',[
            'data' => $data
        ]);

    }

    public function update(Request $req,$id)
    {
        $post = Post::find($id);
        $post->title = $req->title;
        $post->content = $req->content;
        $post->user_id = Auth::user()->id;
        if($req->published == '1'){
            $post->published = '1';
        } else {
            $post->published = '0';
        }

        $post->save();

        return redirect()->route('post');

    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('post');
    }

}
